# mec-server


Regenerae gRPC stubs for Golang
```
protoc --go_out=. --go_opt=paths=source_relative \
    --go-grpc_out=. --go-grpc_opt=paths=source_relative \
    echo/echo.proto
```


Regenerae gRPC stubs for Python
```
python3 -m grpc_tools.protoc -I proto --python_out=. --grpc_python_out=. proto/echo.proto
```