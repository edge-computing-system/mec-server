import echo_pb2
import echo_pb2_grpc
from concurrent import futures
import grpc


class EchoServicer(echo_pb2_grpc.ReplyerService):
    def reply(self, request, context):
        print(f"Received: {request.payload}")
        echo_msg = echo_pb2.EchoReplyMsg()
        echo_msg.payload = f"Hello, {request.payload}"
        print(f"About to reply with {echo_msg.payload}")
        return echo_msg


def serve():
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=1))
    echo_servicer = EchoServicer()
    echo_pb2_grpc.add_ReplyerServiceServicer_to_server(echo_servicer, server)
    server.add_insecure_port("[::]:50051")
    server.start()
    print("Server started")
    server.wait_for_termination()


if __name__ == "__main__":
    serve()
