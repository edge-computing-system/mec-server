package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"net"

	pb "com.example/mec-server/echo"

	"google.golang.org/grpc"
)

var (
	port = flag.Int("port", 50051, "The server port")
)

type server struct {
	pb.UnimplementedReplyerServiceServer
}

func (s *server) Reply(ctx context.Context, in *pb.EchoReplyMsg) (*pb.EchoReplyMsg, error) {
	log.Printf("Received: %v", in.GetPayload())
	return &pb.EchoReplyMsg{Payload: in.GetPayload()}, nil
}

func main() {
	flag.Parse()

	listener, err := net.Listen("tcp", fmt.Sprintf(":%d", *port))

	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	s := grpc.NewServer()

	pb.RegisterReplyerServiceServer(s, &server{})

	log.Printf("server listening at %v", listener.Addr())

	if err := s.Serve(listener); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}
